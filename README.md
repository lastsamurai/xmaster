# gmaster

"Are you looking for an automated script to provision an Ubuntu Linux 18.04 server WITH YOUR ON STARTUP SCRIPT on Google Cloud Platform ?"
gmaster can help you SET EVERYTHING UP AUTOMATICALLY.

USAGE:

* gmaster init
* gmaster deploy [MACHINE-NAME] [STARTUP-SCRIPT]
* gmaster flush


# How to use gmaster

First, run:

```bash
gmaster init

#init command authenticates with your GCP
#or uses the default credentials to MY GCP

```

as a sample startup script, you can find *startup.sh* in this repo.
*startup.sh* configures server hostname and sets up the enviroment required to run [PLAYLIST HTTP API](https://gitlab.com/lastsamurai/playlist) Service on the Ubuntu machine.
[PLAYLIST HTTP API](https://gitlab.com/lastsamurai/playlist) is a dockerized app that we run using docker-compose as the final command in *startup.sh*
Please refer to Playlist's gitlab repo for more information.

## ./gmaster init

The init command makes sure your gcloud SDK is up-to-date and ready-to-go.  It also authenticates with GCP using your own credentials or the default credentials to MY GCP.  
Finally, a list of your VMs is queried and stored locally.

p.s. you MUST first run *gmaster init* before you run either other command.

## ./gmaster deploy [MACHINE-NAME] [STARTUP-SCRIPT]

DESCRIPTION

## ./gmaster flush

DESCRIPTION




* External APIs to which the messages are sent are declared within **ApiController** as the static
array **ApiController::$API_list** so **please make sure you specify your own API URLs**
* To Create/Read/Update/Delete messages, visit **/message** route (or simply the home page)
* Before you send a message, You need to create a contact by visiting the **/contact** route (or simply clicking on _contacts_ tab).
Being that this is a minimal application, you cannot delete or update contacts
* To Send Messages, start with the homepage where you can select/create a message.
Next, you’ll be taken to the contact page where you’ll pick an already registered Contact
& Finally the message will be sent to the external APIs and a report page will be displayed

## Technical Notes
* **Exceptions** are thrown when unexpected or meaningless actions are taken such as _sending an empty message_ or _entering letters instead of digits as contact's phone number_. Normally these exceptions should be caught, however as this is a minimal demo project, exceptions are thrown but not caught ( though, API availability exceptions **are** caught as it's part of the problem description ).
* This project is based on **php7**, **symfony** components,and **mysql**(mariaDB) and does not incorporate caching mechanism with memcached.
* The **Report** page (located at route **/report**) provides a log of all messages sent along with their status and respective External SMS API.
* **
## Contact Me
In case you have questions or comments, do not hesitate to contact me at [caffotinated@gmail.com](mailto:caffotinated@gmail.com)